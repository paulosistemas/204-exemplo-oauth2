package br.com.mastertech.carros;

import br.com.mastertech.carros.security.Usuario;

public class Carro {

    private String modelo;

    private Usuario usuario;

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
